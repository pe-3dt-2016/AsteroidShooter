﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    private Rigidbody RigidBody;

    public Transform BeamSource;
    public Transform BeamDestination;

    public GameObject Beam;
    public float Acceleration = 5.0f;
    public float MaxVelocity = 10.0f;

    void prepareBeam(Vector3 from, Vector3 to)
    {
        Beam.transform.position = (from + to) * 0.5f;

        Quaternion rotation = new Quaternion();
        rotation.SetLookRotation(
            (to - from).normalized
        );
        Beam.transform.rotation = rotation;

        Vector3 scale = Beam.transform.localScale;
        scale.z = Vector3.Distance(from, to) * 0.5f;
        Beam.transform.localScale = scale;
    }

	void Start()
    {
        RigidBody = GetComponent<Rigidbody>();
	}
	
	void Update()
    {
        float horizontal = 
            Input.GetAxis("Horizontal");
        float vertical = 
            Input.GetAxis("Vertical");

        Vector3 force =
            new Vector3(horizontal, 0.0f, vertical);

        RigidBody.AddForce(
            force.normalized * Acceleration
        );

        if (
            RigidBody.velocity.magnitude > MaxVelocity
        )
        {
            RigidBody.velocity =
                RigidBody.velocity.normalized * 
                MaxVelocity;
        }

        Vector3 screenGeometry = new Vector3(
            Screen.width,
            Screen.height,
            0.0f
        );
        Vector3 mouseDirection =
            Input.mousePosition -
            screenGeometry * 0.5f;

        mouseDirection.z = mouseDirection.y;
        mouseDirection.y = 0.0f;

        Quaternion rotation = new Quaternion();
        rotation.SetLookRotation(mouseDirection);
        transform.rotation = rotation;

        Vector3 position = 
            Camera.main.transform.position;
        position.x = transform.position.x;
        position.z = transform.position.z;
        Camera.main.transform.position = position;

        if (Input.GetMouseButton(0))
        {
            Beam.SetActive(true);
            prepareBeam(
                BeamSource.position,
                BeamDestination.position
            );
        }
        else
        {
            Beam.SetActive(false);
        }
    }
}
